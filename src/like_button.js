'use strict';

// eslint-disable-next-line no-undef
const e = React.createElement;

// eslint-disable-next-line no-undef
class LikeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { liked: false };
  }

  render() {
    if (this.state.liked) {
      // eslint-disable-next-line react/prop-types
      return 'You liked comment number ' + this.props.commentID;
    }

    return e(
      'button',
      { onClick: () => this.setState({ liked: true }) },
      'Like'
    );
  }
}

// Find all DOM containers, and render Like buttons into them.
document.querySelectorAll('.like_button_container')
  .forEach(domContainer => {
    // Read the comment ID from a data-* attribute.
    const commentID = parseInt(domContainer.dataset.commentid, 10);
    // eslint-disable-next-line no-undef
    ReactDOM.render(
      e(LikeButton, { commentID: commentID }),
      domContainer
    );
  });
